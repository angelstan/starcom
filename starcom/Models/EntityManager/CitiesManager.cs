﻿using starcom.Models.DB;
using starcom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace starcom.Models.EntityManager
{
    public class CitiesManager
    {
        public void AddCity(CreateCityView cv)
        {
            try
            {
                using (bb5Entities db = new bb5Entities())
                {
                    t03 t03 = new t03();
                    t03.t03_firma = cv.t03_firma;
                    t03.t03_naziv = cv.t03_naziv;
                    t03.t03_naziven = cv.t03_naziven;
                    t03.t03_pbroj = cv.t03_pbroj;
                    t03.t03_opstina = cv.t03_opstina;
                    t03.t03_drzava = Convert.ToInt16(cv.selectedStateId);
                    t03.t03_oznaka = cv.t03_oznaka;
                    t03.t03_user = "test";
                    t03.t03_datu = DateTime.Now;

                    db.t03.Add(t03);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}