﻿using starcom.Models.DB;
using starcom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace starcom.Models.EntityManager
{
    public class StatesManager
    {
        public List<StateList> GetAllStates()
        {
            List<StateList> statesList = new List<StateList>();
            using (bb5Entities db = new bb5Entities())
            {
                StateList state;

                var states = db.t04.ToList();
                foreach (t04 s in states)
                {
                    state = new StateList();
                    state.t04_naziv = s.t04_naziv.ToString();
                    state.t04_sifra = s.t04_sifra;
                    statesList.Add(state);
                }
            }
            return statesList;
        }
        public static IEnumerable<SelectListItem> GetStatesList()
        {
            StatesManager sm = new StatesManager();

            var states = sm.GetAllStates().Select(x => new SelectListItem
            {
                Value = x.t04_sifra.ToString(),
                Text = x.t04_naziv
            });
            return new SelectList(states, "Value", "Text");
        }
    }
}