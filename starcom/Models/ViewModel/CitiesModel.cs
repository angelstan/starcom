﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace starcom.Models.ViewModel
{
    public class CreateCityView
    {
        [Key]
        public int t03_sifra { get; set; }
        public short t03_firma { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Назив")]
        public string t03_naziv { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Назив латиница")]
        public string t03_naziven { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Поштенски број")]
        public string t03_pbroj { get; set; }
        [Display(Name = "Општина")]
        public int t03_opstina { get; set; }
        [Display(Name = "Држава")]
        public int selectedStateId { get; set; }
        public IEnumerable<SelectListItem> states { get; set; }
        [Display(Name = "Ознака")]
        public short t03_oznaka { get; set; }
        //public string t03_user { get; set; }
    }
}