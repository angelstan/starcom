//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starcom.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class t07
    {
        public int t07_id { get; set; }
        public short t07_firma { get; set; }
        public short t07_sifra { get; set; }
        public string t07_naziv { get; set; }
        public string t07_naziven { get; set; }
        public string t07_krat { get; set; }
        public string t07_oznaka { get; set; }
        public string t07_user { get; set; }
        public Nullable<System.DateTime> t07_datu { get; set; }
    }
}
