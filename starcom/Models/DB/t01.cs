//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starcom.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class t01
    {
        public int t01_id { get; set; }
        public short t01_firma { get; set; }
        public int t01_sifra { get; set; }
        public string t01_naziv1 { get; set; }
        public string t01_naziv2 { get; set; }
        public string t01_naziven1 { get; set; }
        public string t01_naziven2 { get; set; }
        public string t01_adresa { get; set; }
        public string t01_adresen { get; set; }
        public int t01_grad { get; set; }
        public string t01_ddv { get; set; }
        public string t01_danbr { get; set; }
        public string t01_emb { get; set; }
        public string t01_ziro { get; set; }
        public string t01_tel { get; set; }
        public string t01_email { get; set; }
        public string t01_www { get; set; }
        public short t01_tip { get; set; }
        public byte t01_vid { get; set; }
        public byte t01_status { get; set; }
        public string t01_user { get; set; }
        public Nullable<System.DateTime> t01_datu { get; set; }
    }
}
