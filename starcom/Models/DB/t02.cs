//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starcom.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class t02
    {
        public int t02_id { get; set; }
        public short t02_firma { get; set; }
        public int t02_sifra { get; set; }
        public string t02_prezime { get; set; }
        public string t02_ime { get; set; }
        public string t02_time { get; set; }
        public byte t02_status { get; set; }
        public string t02_user { get; set; }
        public Nullable<System.DateTime> t02_datu { get; set; }
    }
}
